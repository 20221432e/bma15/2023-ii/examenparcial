/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lsanchezd.cambiodeplanesparcialescrito;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class CambioDePlanesParcialEscrito {

    public static void main(String[] args) {

        int arreglo[] = new int[5];

        arreglo[0] = 78;
        arreglo[1] = 68;
        arreglo[2] = 58;
        arreglo[3] = 71;
        arreglo[4] = 74;
        int i = 0;

        for (int dato : arreglo) {
            if (i == arreglo.length - 1) {
                System.out.print(dato);
            } else {
                System.out.print(dato + ", ");
            }
            i++;
        }

    }
}
