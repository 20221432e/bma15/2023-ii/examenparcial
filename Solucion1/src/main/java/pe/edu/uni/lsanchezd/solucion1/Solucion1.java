/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lsanchezd.solucion1;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class Solucion1 {

    public static void main(String[] args) {

        String string = " Luis Sánchez ";
        System.out.println(string);

        // Length
        System.out.println("length of string: " + string.length());

        // character location
        System.out.println(string.charAt(1));
        System.out.println(string.charAt(string.length() - 2));

        // convert
        System.out.println(string.toUpperCase());

        // split
        String[] partes = string.split("á");
        // visualizando lo que hizo el split
        for (int i = 0; i < partes.length; i++) {
            System.out.println(partes[i]);
        }

        // trim
        String trim = string.trim();
        System.out.println("trim: " + trim);
        System.out.println("length of trim: " + trim.length());

    }
}
