/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lsanchezd.solucion4;

import java.util.Scanner;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class Solucion4 {

    public static void main(String[] args) {
        
        

        Scanner input = new Scanner(System.in);
        float[] num = new float[10];

        System.out.println("Ingrese los numeros: ");
        for (int i = 0; i < num.length; i++) {
            num[i] = input.nextFloat();
        }

        System.out.println("Array: ");
        for (int i = 0; i < num.length; i++) {
            if (i == num.length - 1) {
                System.out.print(num[i]);
            } else {
                System.out.print(num[i] + ", ");
            }
        }

    }
}
