/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lsanchezd.flagsusearrays;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class FlagsUseArrays {

    
    
    public static void main(String[] args) {
//        new FlagsUseArrays().linealSearchOne();
//        new FlagsUseArrays().linealSearchTwo();
        new FlagsUseArrays().orderArray();
    }

    private void orderArray() {
        System.out.println("Ordenar un array de números de manera ascendente");
        int[] numeros = {9, 5, 7, 3, 8, 6, 4, 0, 2, 1};
        for (int i = 0; i < numeros.length - 1; i++) {
            // encontramos la posición del mínimo valor
            int minIndex = i;   // asumir que el valor más pequeño está en la primera posición
            int minValue = numeros[minIndex];
            boolean foundMinValue = false;
            for (int j = i + 1; j < numeros.length; j++) {
                if (minValue > numeros[j]) {
                    minValue = numeros[j];
                    minIndex = j;
                    foundMinValue = true;
                }
            }
            System.out.print("\nMínimo encontrado: " + foundMinValue + ", valor: " + minValue + ", índice: " + minIndex + ", nuevo array: ");
            // intercambiar el valor mínimo con la primera posición
            if (foundMinValue) {
                numeros[minIndex] = numeros[i];
                numeros[i] = minValue;
            }
            // visualizar el array
            for (int j = 0; j < numeros.length; j++) {
                System.out.print(numeros[j] + " ");
            }
        }
        /*
        minIndex = 1;   // asumir que el valor más pequeño está en la primera posición
        minValue = numeros[minIndex];
        for (int i = 2; i < numeros.length; i++) {
            if (minValue > numeros[i]) {
                minValue = numeros[i];
                minIndex = i;
            }
        }
        System.out.println("\nValor mínimo encontrado: " + minValue + ", índice: " + minIndex);
        // intercambiar el valor mínimo con la primera posición
        numeros[minIndex] = numeros[1];
        numeros[1] = minValue;
        // visualizar el array
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + " ");
        }
         */
    }

    private void linealSearchTwo() {
        System.out.println("Búsqueda lineal de los elementos (keys) encontrados en el array");
        int[] array = {1, 4, 8, 5, 2, 9, 4, 8, 5, 7, 11, 2};
        int key = 8;
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                System.out.println("Número encotrado en la posición: " + i);
                found = true;
            }
//            System.out.print(array[i] + " ");
        }
        if (!found) {
            System.out.println("Número no encontrado");
        }
    }

    private void linealSearchOne() {
        System.out.println("Búsqueda lineal de un elemento (key) donde todos los elementos del array son diferentes");
        int[] array = {1, 4, 8, 5, 2, 9, 3};
        int key = 9;

        int index = -1;
        boolean found = false;  // se encontró el elemento
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                index = i;
                found = true;
                break;
            }
        }
        // escribimos la solución
        if (found) {
            System.out.println("elemento encontrado en la posición: " + index);
        } else {
            System.out.println("No se encontró el elemento");
        }
    }

    
}
