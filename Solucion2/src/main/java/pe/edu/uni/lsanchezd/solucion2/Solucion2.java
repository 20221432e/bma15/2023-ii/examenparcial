/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lsanchezd.solucion2;

import java.io.File;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class Solucion2 {

    public static void main(String[] args) {
        File p1 = new File("file.txt");
        File q1 = new File("file.txt");
        File r1 = p1;
        System.out.println("p1 == q1? " + (p1 == q1));
        System.out.println("p1 == r1? " + (p1 == r1));

    }
}
