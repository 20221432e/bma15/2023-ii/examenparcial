/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lsanchezd.orderarray;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class OrderArray {

    public static void main(String[] args) {
        
        int[] arreglo = {5, 2, 9, 0, 5, 65, -89};
        int n = arreglo.length;

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1 - i; j++) {
                if (arreglo[j] > arreglo[j + 1]) {
                    // Intercambiar elementos si están en el orden incorrecto.
                    int temp = arreglo[j];
                    arreglo[j] = arreglo[j + 1];
                    arreglo[j + 1] = temp;
                }
            }
        }

        // Imprimir el arreglo ordenado
        for (int elemento : arreglo) {
            System.out.print(elemento + " ");
        }
    }
}

    

