/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lsanchezd.arraysprivateclass;

import java.util.Random;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class ArraysPrivateClass {

    public static void main(String[] args) {
           //  new ArraysPrivateClass().contarNumeros();
        new ArraysPrivateClass().contarLetras();

    }
    
    
    private void contarLetras() {
        // https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html codigo ASCII
        System.out.println("Contar letras mínusculas en un Array !!!");
        // inicializar
        char[] chars = {'H', 'o', 'l', 'a'};
        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i]);
        }
        System.out.println("");
        System.out.println(chars[chars.length - 1]);
        System.out.println((int) chars[chars.length - 1]);
        System.out.println((char) ('a' + 1));

        char[] caracteres = new char[100];
        for (int i = 0; i < caracteres.length; i++) {
            Random r = new Random();
            char c = (char) (r.nextInt(26) + 'a');
            caracteres[i] = c;
        }
        // visualizar
        for (int i = 0; i < caracteres.length; i++) {
            System.out.print(caracteres[i]);
        }
        System.out.println("");
        // repeteciones
        int[] contadores = new int[26];
        // [a, b, c, d, ... z]
        for (int i = 0; i < caracteres.length; i++) {
            contadores[caracteres[i] - 'a']++;
        }
        // visualizar
        for (int i = 0; i < contadores.length; i++) {
            System.out.print((char)(i + 'a') + "," + contadores[i] + " ");
        }
    }

    private void contarNumeros() {
        System.out.println("Contar números mayores al promedio del conjunto!!!");

        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese un número de ítems: ");
        int n = input.nextInt();
        double[] numeros = new double[n];
        double suma = 0;
        System.out.println("Ingrese los números:");
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = input.nextDouble();
            suma += numeros[i];
        }

        double promedio = suma / numeros.length;

        int contador = 0;
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] > promedio) {
                contador++;
            }
        }
        System.out.println("promedio: " + promedio);
        System.out.println("Número de elementos mayores al promedio: " + contador);

    }

    
    
}
