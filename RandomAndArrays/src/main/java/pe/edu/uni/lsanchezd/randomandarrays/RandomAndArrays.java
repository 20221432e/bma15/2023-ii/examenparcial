/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lsanchezd.randomandarrays;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class RandomAndArrays {

    public static void main(String[] args) {
        
        
        System.out.println("Arrays!");
        int arraySize = 5;
        double[] styleJavaArray;
        double styleCppArray[];

        styleJavaArray = new double[arraySize];
        styleCppArray = new double[arraySize];

        // [0.0,0.0,0.0,0.0,0.0]
        styleJavaArray[0] = 5;
        styleJavaArray[1] = 4;
        styleJavaArray[2] = 3;
        styleJavaArray[3] = 2;
        styleJavaArray[4] = 1;

        System.out.println("styleJavaArray[2]: " + styleJavaArray[2]);
        System.out.println("Visualizando:");
        for (int i = 0; i < styleJavaArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + styleJavaArray[i]);
        }
        System.out.println("Definiendo");
        for (int i = 0; i < styleJavaArray.length; i++) {
            styleJavaArray[i] = i;
        }
        System.out.println("Visualizando:");
        for (int i = 0; i < styleJavaArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + styleJavaArray[i]);
        }

        // inicializando forma 2
        double[] myArray = {1.9, 2.9, 3.4, 3.5, 7.2};
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }
        /*
        System.out.println("escribir nuevos valores");
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese " + myArray.length + " valores: ");
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = input.nextDouble();
        }
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }
         */
        // inicializando valores aleatorios
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = Math.random() * 100;
        }
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }

        // suma
        double total = 0;
        for (int i = 0; i < myArray.length; i++) {
            total += myArray[i];
        }
        System.out.println("total: " + total);

        // Encontrar el elemento más grande
        double max = myArray[0];
        int indexOfMax = 0;
        for (int i = 1; i < myArray.length; i++) {
            if (myArray[i] < max) {
                max = myArray[i];
                indexOfMax = i;
            }
        }
        System.out.println("max: " + max);
        System.out.println("Index of max: " + indexOfMax);

        // mezclar elementos
        for (int i = 0; i < myArray.length; i++) {
            // generar un index aleatorio
            int j = (int) (Math.random() * myArray.length);
//            System.out.println("j:" + j);
            double temp = myArray[i];
            myArray[i] = myArray[j];
            myArray[j] = temp;
        }
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }

        // Shifting a la izquierda
        // [10, 20, 30, 40, 50]
        // [20, 30, 40, 50, 10]
        double temp = myArray[0];   // retiene el primer elemento
        for (int i = 1; i < myArray.length; i++) {
            myArray[i - 1] = myArray[i];
        }
        myArray[myArray.length - 1] = temp;
        System.out.println("Visualizando:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "] = " + myArray[i]);
        }

        //
        String[] months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingresa un número del mes (1 al 12): ");
        int monthNumber = input.nextInt();
        System.out.println("El mes es " + months[monthNumber - 1]);

        
        
    }
}
